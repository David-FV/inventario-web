<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import = "Model.Producto" %>
<jsp:useBean id = "lista" scope = "session" class = "java.util.List" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilos.css">
        <link href="SA/sweetalert2.css" rel="stylesheet" type="text/css"/>
        <title>Control de inventario</title>
    <%@include file= "../WEB-INF/Vistas-Parciales/css-jsp.jspf"  %>
         </head>
    <body>
        <%@include file= "../WEB-INF/Vistas-Parciales/encabezado.jspf" %>

        <div class="container-mostrar mx-auto bg-info border-dark shadow p-3 mb-5 rounded-3">
            <a href="<%= request.getContextPath() %>/productos?opcion=crear" class="btn btn-success btn-sm glyphicon glyphicon-pencil" role="button"> Nuevo Producto</a>
             <h1 class="text-dark" align="center">Listado de Productos Registrados</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">ID</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">NOMBRE</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">Stocks</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">Precio</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">Unidad</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">ESTADO</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">CATEGORIA</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">ACCION</th>
                    </tr>
                </thead>
                
                <%
                    for(int i=0 ; i < lista.size(); i++){
                        Producto producto = new Producto();
                        producto = (Producto)lista.get(i);
                %>
                <tr>
                    <td><%= producto.getId_producto() %></td>
                    <td><%= producto.getNom_producto() %></td>
                    <td><%= producto.getStock() %></td>
                    <td><%= producto.getPrecio() %></td>
                    <td><%= producto.getUnidadMedida() %></td>
                    <td><%= producto.getEstado() %></td>
                    <td><%= producto.getCategoria() %></td>
                     <td>
                        <a href="<%= request.getContextPath()%>/productos?opcion=modificar&&id_product=<%= producto.getId_producto()%>&&nom_product=<%= producto.getNom_producto()%>&&stock_product=<%= producto.getStock() %>&&precio_product=<%= producto.getPrecio() %>&&unidadmedida_product=<%= producto.getUnidadMedida() %>&&estado_product=<%= producto.getEstado()%>&&cat_prod=<%= producto.getCategoria() %>"
                           
                           class="btn btn-warning btn-sm glyphicon glyphicon-edit" role="button" name="btnmodi" data-toggle="modal" data_target = "#largeModal">Editar</a>
                        
                           <!--Eliminar-->
                        <a href="<%= request.getContextPath()%>/productos?opcion=eliminar&&id_product=<%= producto.getId_producto()%>" class="btn btn-danger btn-sm glyphicon glyphicon-remove" role="button" name="btneli">Eliminar</a>                        
                    </td>
                </tr>
                <%
                    }
                %>
                
            </table>
        </div>
        
        <%@include file= "../WEB-INF/Vistas-Parciales/pie.jspf" %>
    </body>
</html>


