<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="producto" scope="session" class="Model.Producto"/>

<% 
    String id_prod = "";
    String nombre_prod = "";
    int cate_id = 0;
    float stock_prod = 0;
    float precio_prod = 0;
    String unidadMedida_prod = "";
    int estado_prod = 0;
    //categoria
    
    if(request.getParameter("senal") != null){
        
        id_prod = request.getParameter("id_p"); //parentesis viene de la clase productos
        nombre_prod = request.getParameter("nombre_p");
        cate_id = Integer.parseInt(request.getParameter("catego_p"));
        stock_prod = Float.valueOf(request.getParameter("stock_p"));
        precio_prod = Float.valueOf(request.getParameter("precio_p"));
        unidadMedida_prod = request.getParameter("unidadMedida_p");
        estado_prod = Integer.parseInt(request.getParameter("estado_p"));
        
        
              /*  out.print("ID: " + id_prod);
                out.print("NOMBRE: " + nombre_prod );
                out.print("CATE_ID: " + cate_id);
                out.print("STOCK: " + stock_prod);
                out.print("PRECIO: " + precio_prod);
                out.print("U. Medida: " + unidadMedida_prod);
                out.print("Estdado: " + estado_prod);      */          
            
        
    }else{
        id_prod = String.valueOf(producto.getId_producto());
        nombre_prod = producto.getNom_producto();
        //cate_id = Integer.valueOf(producto.getCategoria_id()); //x
        stock_prod = Float.valueOf(producto.getStock());
        precio_prod = Float.valueOf(producto.getPrecio());
        unidadMedida_prod = producto.getUnidadMedida();
        estado_prod = Integer.valueOf(producto.getEstado());   //x    
        
               /* out.print("ID: " + id_prod);
                out.print("NOMBRE: " + nombre_prod );
                out.print("CATE_ID: " + cate_id);
                out.print("STOCK: " + stock_prod);
                out.print("PRECIO: " + precio_prod);
                out.print("U. Medida: " + unidadMedida_prod);
                out.print("Estdado: " + estado_prod);  */
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Control de Inventario</title>
        <link rel="stylesheet" href="css/estilos.css">
        <%@include file = "../WEB-INF/Vistas-Parciales/css-jsp.jspf" %>
        <script type="text/javascript">
            function regresar(url) {
                location.href = url;
            }
        </script>
    </head>
    <body>
       <%@include file= "../WEB-INF/Vistas-Parciales/encabezado.jspf" %>

       <div class="caja-product position-absolute top-50 start-50 translate-middle bg-dark  shadow p-3 mb-5 rounded">
            <h1 class="py-2 text-light text-center" >Productos</h1>
            <hr class="bg-light" >
            <form class="py-3 m-2" id="frmProducto" name="frmProducto" action="<%= request.getContextPath()%>/productos" method="post">
                <input type="hidden" name="id_producto" value="<%= id_prod %>">
                       
                <label class="label-product text-light">Nombre:</label><input type="text" class="inp-product border-dark border-2" name="NomProducto" value="<%=  nombre_prod %>" style="width: 200px;  margin-left: 14px;">
                <br>
                <label class="label-product text-light">Stock:</label><input type="text" class="inp-product border-dark border-2" name="StockProducto" value="<%=  stock_prod %>" style="width: 150px; margin-left: 40px;">
                <br>
                <label class="label-product text-light" >Precio:</label><input type="text" class="inp-product border-dark border-2" name="PrecProducto" value="<%=  precio_prod %>" style="width: 150px;  margin-left: 34px;">
                <br>
                <label class="label-product text-light" >Unidad:</label><input type="text" class="inp-product border-dark border-2" name="UnidadProducto" value="<%=  unidadMedida_prod %>" style="width: 150px;  margin-left: 24px;">
                <br>
                <label class="label-product text-light">Estado:</label><input type="text" class="inp-product border-dark border-2" name="EstadoProducto" value="<%=  estado_prod %>" style="width: 200px;  margin-left: 30px;">
                <br>
                <label class="label-product text-light">Categoria:</label><input type="text" class="inp-product border-dark border-2" name="CatProducto" value="<%=  cate_id %>" style="width: 200px;  margin-left: 2px;">
                
                <br><br>
                  <center>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <%
                              if(request.getParameter("senal") != null){                              
                            %>
                            <input type="submit" class="btn btn-danger btn-sm" name="btnModificar" value="Actualizar"/>
                            <%                                                                   
                                }else{
                            %>                            
                            <input type="submit" class="btn btn-success btn-sm" name="btnGuardar" value="Guardar" />
                            <%
                            }
                            %>
                            <input type="button" class="btn btn-danger btn-lm" onclick="regresar('<%= request.getContextPath()%>/productos?opcion=listar')" name="btnRegresar" value="Regresar" />
                        </div>
                    </div>
                </center>
                
           </form>
        </div> 
       
         <%@include file= "../WEB-INF/Vistas-Parciales/pie.jspf" %>
    </body>
</html>