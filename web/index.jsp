<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="css/estilos.css">
        <%@include file= "WEB-INF/Vistas-Parciales/css-jsp.jspf"  %>
    </head>
    <body class="fondo">
      
        <div class="position-absolute top-50 start-50 translate-middle">
            <form action="" method="post">
              <i class="fas fa-user-circle fa-5x d-flex justify-content-center"  style="color: white"></i>
              <h1 class="text-center text-light p-2">Bienvenido</h1>
              <hr class="bg-light">
              
              <div class="box-text">
                   <i class="far fa-user fa-1x "  style="color: white"></i>
                   <input required="" placeholder="Usuario..." name="user" type="text">
              </div>
              
              <div class="box-text">
                  <i class="fas fa-lock fa-1x" style="color: white"></i>
                  <input required="" placeholder="Contraseña..." name="contra" type="password">
              </div>
          
                  <button class="position-relative bottom-0 start-50 translate-middle-x btn btn-success btn-lg py-2 buton-login ">
                      <a href="categorias?opcion=listar">Ingresar</a>
                  </button>
            </form>
          </div>
            <%@include file= "WEB-INF/Vistas-Parciales/pie.jspf" %>
         </body>
</html>
