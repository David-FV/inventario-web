package Test;

import DAO.CategoriaDAO;
import DAO.CategoriaDAOImplementar;
import DAO.ProductoDAO;
import DAO.ProductoDAOImplementar;
import Model.Categoria;
import Model.Producto;
import java.util.List;

public class Prueba {
    
    public static void main(String[] args) {
        
        //Aqui realizaremos las pruebas
        Prueba evaluar = new Prueba();
        //evaluar.elimanarCategoria();        
        //evaluar.guardaProducto();
        //evaluar.listarProductos();
    }
    
    //Metodo para eliminar un registro
    public void elimanarCategoria () {
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        categoria.borrarCat(6);
    }
    
    public void listarProductos(){
        ProductoDAO producto = new ProductoDAOImplementar();
        List<Producto> listar = producto.Listar();
        System.out.println("Listado de productos");
        
        for(Producto productoListar : listar){
            System.out.println("ID: " + productoListar.getId_producto() +
                               " Nombre: " + productoListar.getNom_producto() +
                               " Stock: " + productoListar.getStock() + 
                               " Precio: " + productoListar.getPrecio() +
                               " U. Medida: " + productoListar.getUnidadMedida() +
                               " Estado: " + productoListar.getEstado() +
                               " Categoria: " + productoListar.getCategoria());
        }
    }
    
    public void guardaProducto(){
        ProductoDAO producto = new ProductoDAOImplementar();
        Producto guardarProducto = new Producto();
        
        guardarProducto.setNom_producto("TV");
        guardarProducto.setStock(10);
        guardarProducto.setPrecio(100);
        guardarProducto.setUnidadMedida("XL");
        guardarProducto.setEstado(1);
        guardarProducto.setCategoria("33");
        producto.guardarProd(guardarProducto);
    }
}
