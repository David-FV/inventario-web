package Controller;

import DAO.ProductoDAO;
import DAO.ProductoDAOImplementar;
import Model.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Productos extends HttpServlet {

    protected void listarProductos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        ProductoDAO producto = new ProductoDAOImplementar();
        HttpSession sesion = request.getSession(true);
        sesion.setAttribute("lista", producto.Listar());
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Vistas-Productos/listarProductos.jsp");
        dispatcher.forward(request, response);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String parametro = request.getParameter("opcion");
        
        if(parametro.equals("crear")){
            
            String pagina = "/Vistas-Productos/crearProducto.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
            
        }else if(parametro.equals("listar")){
            
            this.listarProductos(request, response);
            
        }else if(parametro.equals("modificar")){
            //se efectua el casting o conversion de datos por que lo ingresado en el formulario es texto
            int id_producto = Integer.parseInt(request.getParameter("id_product"));
            String nom_producto = request.getParameter("nom_product");
            float stock_producto = Float.valueOf(request.getParameter("stock_product"));
            float precio_producto = Float.valueOf(request.getParameter("precio_product"));
            String unidadMedida_producto = request.getParameter("unidadmedida_product");            
            int estado_producto = Integer.parseInt(request.getParameter("estado_product"));            
            String cat_producto = request.getParameter("cat_prod");
            
            String pagina = "/Vistas-Productos/crearProducto.jsp?id_p="+id_producto+"&&nombre_p="+nom_producto+"&&stock_p="+stock_producto+"&&precio_p="+precio_producto+"&&unidadMedida_p="+unidadMedida_producto+"&&estado_p="+estado_producto+"&&catego_p="+cat_producto+"&&senal=1";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
            
            /*response.setContentType("text/html;charset=UTF-8");
            try(PrintWriter out = response.getWriter()){
                System.out.println("ID: " + id_producto);
                System.out.println("NOMBRE: " + nom_producto );
                System.out.println("CATE_ID: " + cat_id);
                System.out.println("STOCK: " + stock_producto);
                System.out.println("PRECIO: " + precio_producto);
                System.out.println("U. Medida: " + unidadMedida_producto);
                System.out.println("Estdado: " + estado_producto);
                
            } catch (Exception e) {
            }*/
        }else if(parametro.equals("eliminar")){
            int del_id = Integer.parseInt(request.getParameter("id_product"));
            ProductoDAO producto = new ProductoDAOImplementar();
            producto.borrarProd(del_id);
            this.listarProductos(request, response);
        }
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
try{
            Producto producto = new Producto();
        
            producto.setId_producto(Integer.parseInt(request.getParameter("id_producto")));
            producto.setNom_producto(request.getParameter("NomProducto"));
            producto.setStock(Float.parseFloat(request.getParameter("StockProducto")));
            producto.setPrecio(Float.parseFloat(request.getParameter("PrecProducto")));
            producto.setUnidadMedida(request.getParameter("UnidadProducto"));
            producto.setEstado(Integer.parseInt(request.getParameter("EstadoProducto")));
            producto.setCategoria(request.getParameter("CatProducto"));

            ProductoDAO guardar = new ProductoDAOImplementar();
            guardar.guardarProd(producto);
            this.listarProductos(request, response);
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }  
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
