package Controller;

import DAO.CategoriaDAO;
import DAO.CategoriaDAOImplementar;
import Model.Categoria;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Giovanni López
 */
public class Categorias extends HttpServlet {
    
    //Metodo listaCategorias
    protected void listaCategorias(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        //Se creo la instancia a CategoriaDAO
        CategoriaDAO categoria = new CategoriaDAOImplementar();
        //Se creo la instancia de sesión; se le da true para crear la sesión
        HttpSession sesion = request.getSession(true);
        sesion.setAttribute("lista", categoria.Listar()); // Lista es el nombre de la sesión
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Vistas-Categorias/listarCategorias.jsp");
        dispatcher.forward(request, response);
        
    }   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String parametro = request.getParameter("opcion"); //Capturar el parametro que se esta enviando
        if(parametro.equals("crear")){ //Evaluar si el parametro es crear o listar o cualquier otro
            String pagina = "/Vistas-Categorias/crearCategoria.jsp"; //Vista o formulario para registrar nueva categoria
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
            
            
        }else if(parametro.equals("listar")){
         //Metodo llamado
            this.listaCategorias(request, response);   
        }else if(parametro.equals("modificar")){
            //se efectua el casting o conversion de datos por que lo ingresado en el formulario es texto
            int id_categoria = Integer.parseInt(request.getParameter("id_cat"));
            String nom_categoria = request.getParameter("nombre_cat");
            int estado_categoria = Integer.parseInt(request.getParameter("estado_cat"));
            
            String pagina = "/Vistas-Categorias/crearCategoria.jsp?id_c="+id_categoria+"&&nombre_c="+nom_categoria+"&&estado_c="+estado_categoria+"&&senal=1";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
                              
        }else if(parametro.equals("eliminar")){
            int del_id = Integer.parseInt(request.getParameter("id_cat"));
            CategoriaDAO categoria = new CategoriaDAOImplementar();
            categoria.borrarCat(del_id);
            this.listaCategorias(request, response);
            
        }
        
        
        
        /*String parametro = request.getParameter("opcion");
        String id_cat = request.getParameter("id_cat");
        String nombre_Cat = request.getParameter("nombre_cat");
        String estado_cat = request.getParameter("estado_cat");
        
        response.setContentType("text/html;charset=UTF-8");
        try(PrintWriter out = response.getWriter()){
            System.out.println(parametro);
            System.out.println(id_cat);
            System.out.println(nombre_Cat);
            System.out.println(estado_cat);
        }*/
    }

    
    //método para guardar
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Categoria categoria = new Categoria();
        //Se efectua el casting o conversion de datos orque lo ingresado en el formulario es texto
        categoria.setId_categoria(Integer.parseInt(request.getParameter("id_categoria")));
        categoria.setNom_categoria(request.getParameter("txtNomCategoria"));
        categoria.setEstado_categoria(Integer.parseInt(request. getParameter("txtEstadoCategoria")));                
        
        CategoriaDAO guardarCategoria = new CategoriaDAOImplementar();
        guardarCategoria.guardarCat(categoria);
        this.listaCategorias(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
